<%@ include file="/WEB-INF/layouts/include.jsp" %>
<h1>O'Reilly Snacks</h1>
<div class="row">
    <div class="col-12">
        <orly-table id="userTable" class="invisible" loaddataoncreate url='<c:url value="/widget/getwidgets" />'
     includefilter bordered maxrows="10" tabletitle="Search Results" class="invisible">
    <orly-column field="action" label="Action" class=""></orly-column>
    <orly-column field="id" label="Snack ID" class="" sorttype="natural"></orly-column>
    <orly-column field="name" label="Name"></orly-column>
    <orly-column field="quantity" label="Quantity"></orly-column>
    <orly-column field="stocked_date" label="Stocked Date"></orly-column>
</orly-table>

    </div>
</div>
