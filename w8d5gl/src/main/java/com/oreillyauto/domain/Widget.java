package com.oreillyauto.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="snacks")
public class Widget implements Serializable {
    private static final long serialVersionUID = 1L;

    public Widget() {
    }
    
    public Widget(Integer id, String name, Integer quantity,Timestamp stocked_date) {
        super();
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.stocked_date = stocked_date;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", columnDefinition="INTEGER")
    private Integer id;
    
    @Column(name="name", columnDefinition="VARCHAR(256)")
    private String name;
    
    @Column(name="quantity", columnDefinition="INTEGER")
    private Integer quantity;
    
    @Column(name="stocked_date", columnDefinition="TIMESTAMP")
    private Timestamp stocked_date;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Timestamp getStocked_date() {
        return stocked_date;
    }

    public void setStocked_date(Timestamp stocked_date) {
        this.stocked_date = stocked_date;
    }
    
    
}
