package com.oreillyauto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.WidgetRepository;
import com.oreillyauto.domain.Widget;
import com.oreillyauto.service.WidgetService;

@Service
public class WidgetServiceImpl implements WidgetService {
    
    @Autowired
    WidgetRepository widgetRepository;

    @Override
    public List<Widget> getWidgets() {
        return (List<Widget>) widgetRepository.findAll();
    }

}