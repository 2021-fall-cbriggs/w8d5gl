package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Widget;

public interface WidgetService {
    List<Widget> getWidgets();
}
