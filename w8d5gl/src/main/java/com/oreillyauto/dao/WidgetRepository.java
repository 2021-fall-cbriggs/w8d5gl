package com.oreillyauto.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.WidgetRepositoryCustom;

import com.oreillyauto.domain.Widget;

public interface WidgetRepository extends CrudRepository<Widget, Integer>, WidgetRepositoryCustom {

}