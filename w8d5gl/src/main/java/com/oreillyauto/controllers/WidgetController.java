package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Widget;
import com.oreillyauto.service.WidgetService;

@Controller
public class WidgetController {
    @Autowired
    WidgetService widgetService;
    
    @GetMapping(value="/widget")
    public String widget(Model model) {
        model.addAttribute("active", "widget");
        return "widget";
    }
    
    @ResponseBody
    @GetMapping(value="/widget/getwidgets")
    public List<Widget> getWidgetsData(){
        return widgetService.getWidgets();
    }
}
