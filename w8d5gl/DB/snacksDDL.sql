CREATE TABLE snacks (
	id INTEGER AUTO_INCREMENT,
    name VARCHAR(256),
    quantity INTEGER(10),
    stocked_date TIMESTAMP,
    PRIMARY KEY(id)
);